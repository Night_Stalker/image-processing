import cv2
import dlib
import numpy as np

cap = cv2.VideoCapture(0)
width = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
height = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
writer = cv2.VideoWriter('video/video_1.mp4', cv2.VideoWriter_fourcc(*'DIVX'), 30, (width, height))

# Load the detector and predictor
detector = dlib.get_frontal_face_detector()
predictor = dlib.shape_predictor("shape_predictor_68_face_landmarks.dat")

while True:
    ret, frame = cap.read()
    if not ret:
        break

    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    faces = detector(gray)

    for face in faces:
        landmarks = predictor(gray, face)

        points = np.array([[p.x, p.y] for p in landmarks.parts()], np.int32)
        hull = cv2.convexHull(points)

        mask = np.zeros_like(gray)
        cv2.fillConvexPoly(mask, hull, 255)

        frame_blurred = cv2.GaussianBlur(frame, (99, 99), 30)
        frame = np.where(mask[:, :, None].astype(bool), frame_blurred, frame)

    cv2.imshow("Output", frame)
    writer.write(frame)

    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

cap.release()
writer.release()
cv2.destroyAllWindows()
