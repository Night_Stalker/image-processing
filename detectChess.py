import cv2

cap = cv2.VideoCapture(0)
width = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
height = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
writer = cv2.VideoWriter('chessDetectVideos/video_1.mp4', cv2.VideoWriter_fourcc(*'DIVX'), 30, (width, height))

while True:
    ret, frame = cap.read()

    convertedFrame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
    found, corners_chess = cv2.findChessboardCorners(convertedFrame, (7, 7))
    cv2.drawChessboardCorners(convertedFrame, (7, 7), corners_chess, found)

    convertedFrame = cv2.cvtColor(convertedFrame, cv2.COLOR_RGB2BGR)
    writer.write(convertedFrame)
    cv2.imshow('detect', convertedFrame)

    if cv2.waitKey(1) & 0xFF == ord('q'):
        break
cap.release()
writer.release()
cv2.destroyAllWindows()
