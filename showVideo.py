import cv2

cap = cv2.VideoCapture('chessDetectVideos/video_1.mp4')
if cap.isOpened() == False:
    print('Error file not found or wrong codec used')
while cap.isOpened():
    ret, frame = cap.read()
    if ret == True:
        cv2.imshow('frame', frame)
        if cv2.waitKey(10) & 0xFF == ord('q'):
            break
    else:
        break
cap.release()
cv2.destroyAllWindows()
