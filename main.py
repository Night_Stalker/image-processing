import cv2

cap = cv2.VideoCapture(0)
width = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
height = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
writer = cv2.VideoWriter('videos/BINARY.mp4', cv2.VideoWriter_fourcc(*'DIVX'), 30, (width, height))

while True:
    ret, frame = cap.read()
    grayVideo = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    _, thresholded_frame = cv2.threshold(grayVideo, 155, 255, cv2.THRESH_BINARY)

    writer.write(cv2.cvtColor(thresholded_frame, cv2.COLOR_GRAY2BGR))
    cv2.imshow('frame', thresholded_frame)

    if cv2.waitKey(1) & 0xFF == ord('q'):
        break
cap.release()
writer.release()
cv2.destroyAllWindows()

