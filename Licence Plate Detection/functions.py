import cv2
from matplotlib import pyplot as plt


def show_img(image, plt_size=8):
    fig = plt.figure(figsize=(plt_size, plt_size))
    ax = fig.add_subplot(111)
    ax.imshow(image)


def show_img_gray(image, plt_size=8):
    fig = plt.figure(figsize=(plt_size, plt_size))
    ax = fig.add_subplot(111)
    ax.imshow(image, cmap='gray')


def show_three_image(images):
    fig = plt.figure(figsize=(30, 30))
    i = 1
    for image in images:
        ax = fig.add_subplot(130 + i)
        ax.imshow(image)
        i += 1


def blur_license_plate(image, x, y, w, h):
    plate_image = image[y:y + h, x:x + w]
    blurred_roi = cv2.GaussianBlur(plate_image, (101, 101), 50)
    image[y:y + h, x:x + w] = blurred_roi
    return image


def detect_and_blur(image_path):
    image = cv2.imread(image_path)

    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    blurred = cv2.bilateralFilter(gray, 11, 17, 17)
    edges = cv2.Canny(blurred, 50, 150)

    contours, _ = cv2.findContours(edges.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    contours = sorted(contours, key=cv2.contourArea, reverse=True)

    middle_image = image.copy()
    cv2.drawContours(middle_image, contours, -1, (0, 255, 0), 5)

    found = False
    for contour in contours:
        perimeter = cv2.arcLength(contour, True)
        approx = cv2.approxPolyDP(contour, 0.015 * perimeter, True)

        if len(approx) == 4:
            x, y, w, h = cv2.boundingRect(contour)
            # found = True
            # cv2.rectangle(image, (x, y), (x + w, y + h), (255, 0, 0), 10)
            # cv2.rectangle(middle_image, (x, y), (x + w, y + h), (255, 0, 0), 10)
            # image = blur_license_plate(image, x, y, w, h)
            aspect_ratio = w / float(h)
            if 1.5 < aspect_ratio and 3000 < cv2.contourArea(contour):
                found = True
                cv2.rectangle(image, (x, y), (x + w, y + h), (255, 0, 0), 10)
                cv2.rectangle(middle_image, (x, y), (x + w, y + h), (255, 0, 0), 10)
                image = blur_license_plate(image, x, y, w, h)

    if found:
        print("License plate found.")
    else:
        print("License plate not found.")

    images = [edges, middle_image, image]
    show_three_image(images)

    return image
    # If no license plate is found, return None
    # return None


def cascade_detect(image_path):
    pretrained = 'haarcascade_russian_plate_number.xml'
    plate_cascade = cv2.CascadeClassifier(pretrained)
    image = cv2.imread(image_path)

    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    plate = plate_cascade.detectMultiScale(gray, scaleFactor=1.05, minNeighbors=5, minSize=(40, 40))

    count = 0
    detected_img = image
    for (x, y, w, h) in plate:
        cv2.rectangle(detected_img, (x, y), (x + w, y + h), (0, 0, 255), 2)  # red bounding box
        cv2.putText(detected_img, str(count), (x, y), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 0), 2, cv2.LINE_AA)
        count += 1

    return detected_img


def detect_and_blur_video(frame):
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    blurred = cv2.bilateralFilter(gray, 11, 17, 17)
    edges = cv2.Canny(blurred, 50, 150)

    contours, _ = cv2.findContours(edges.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    contours = sorted(contours, key=cv2.contourArea, reverse=True)
    for contour in contours:
        perimeter = cv2.arcLength(contour, True)
        approx = cv2.approxPolyDP(contour, 0.015 * perimeter, True)

        if len(approx) == 4:
            x, y, w, h = cv2.boundingRect(contour)
            aspect_ratio = w / float(h)
            if 1 < aspect_ratio and 2000 < cv2.contourArea(contour):
                cv2.rectangle(frame, (x, y), (x + w, y + h), (255, 0, 0), 10)
                frame = blur_license_plate(frame, x, y, w, h)

    return frame
