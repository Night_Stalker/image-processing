import glob

import cv2

import functions as fc

out_path = ['video_1.mp4', 'video_2.mp4', 'video_3.mp4']

video_paths = glob.glob('videos/*.mp4')

i = 0
for video in video_paths:
    cap = cv2.VideoCapture(video)
    width = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
    height = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
    writer = cv2.VideoWriter('detected videos/' + out_path[i], cv2.VideoWriter_fourcc('m', 'p', '4', 'v'), 30,
                             (width, height))
    i = i + 1

    if not cap.isOpened():
        print("Error: Could not open video.")
        exit()

    while cap.isOpened():
        ret, frame = cap.read()
        if not ret:
            break
        detected_frame = img = fc.detect_and_blur_video(frame)
        cv2.imshow('Frame', detected_frame)

        writer.write(detected_frame)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

    cap.release()
    cv2.destroyAllWindows()
